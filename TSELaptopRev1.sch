<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Teensy_3_and_LC_Series_Boards_v1.4">
<packages>
<package name="TEENSY_3.5_DIL">
<pad name="GND" x="-7.62" y="29.21" drill="0.9652"/>
<pad name="0" x="-7.62" y="26.67" drill="0.9652"/>
<pad name="1" x="-7.62" y="24.13" drill="0.9652"/>
<pad name="2" x="-7.62" y="21.59" drill="0.9652"/>
<pad name="3" x="-7.62" y="19.05" drill="0.9652"/>
<pad name="4" x="-7.62" y="16.51" drill="0.9652"/>
<pad name="5" x="-7.62" y="13.97" drill="0.9652"/>
<pad name="6" x="-7.62" y="11.43" drill="0.9652"/>
<pad name="7" x="-7.62" y="8.89" drill="0.9652"/>
<pad name="8" x="-7.62" y="6.35" drill="0.9652"/>
<pad name="9" x="-7.62" y="3.81" drill="0.9652"/>
<pad name="10" x="-7.62" y="1.27" drill="0.9652"/>
<pad name="11" x="-7.62" y="-1.27" drill="0.9652"/>
<pad name="12" x="-7.62" y="-3.81" drill="0.9652"/>
<pad name="24" x="-7.62" y="-8.89" drill="0.9652"/>
<pad name="25" x="-7.62" y="-11.43" drill="0.9652"/>
<pad name="26" x="-7.62" y="-13.97" drill="0.9652"/>
<pad name="27" x="-7.62" y="-16.51" drill="0.9652"/>
<pad name="13" x="7.62" y="-3.81" drill="0.9652"/>
<pad name="14/A0" x="7.62" y="-1.27" drill="0.9652"/>
<pad name="15/A1" x="7.62" y="1.27" drill="0.9652"/>
<pad name="16/A2" x="7.62" y="3.81" drill="0.9652"/>
<pad name="17/A3" x="7.62" y="6.35" drill="0.9652"/>
<pad name="18/A4" x="7.62" y="8.89" drill="0.9652"/>
<pad name="19/A5" x="7.62" y="11.43" drill="0.9652"/>
<pad name="20/A6" x="7.62" y="13.97" drill="0.9652"/>
<pad name="21/A7" x="7.62" y="16.51" drill="0.9652"/>
<pad name="22/A8" x="7.62" y="19.05" drill="0.9652"/>
<pad name="23/A9" x="7.62" y="21.59" drill="0.9652"/>
<pad name="3.3V" x="7.62" y="24.13" drill="0.9652"/>
<pad name="AGND" x="7.62" y="26.67" drill="0.9652"/>
<pad name="VIN" x="7.62" y="29.21" drill="0.9652"/>
<wire x1="-8.89" y1="30.48" x2="8.89" y2="30.48" width="0.127" layer="51"/>
<wire x1="8.89" y1="30.48" x2="8.89" y2="-30.48" width="0.127" layer="51"/>
<wire x1="8.89" y1="-30.48" x2="-8.89" y2="-30.48" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-30.48" x2="-8.89" y2="30.48" width="0.127" layer="51"/>
<wire x1="-3.81" y1="31.75" x2="3.81" y2="31.75" width="0.2032" layer="21"/>
<wire x1="3.81" y1="31.75" x2="3.81" y2="30.48" width="0.2032" layer="21"/>
<wire x1="3.81" y1="30.48" x2="8.89" y2="30.48" width="0.2032" layer="21"/>
<wire x1="8.89" y1="30.48" x2="8.89" y2="-30.48" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-30.48" x2="-8.89" y2="-30.48" width="0.2032" layer="21"/>
<wire x1="-8.89" y1="-30.48" x2="-8.89" y2="30.48" width="0.2032" layer="21"/>
<wire x1="-8.89" y1="30.48" x2="-3.81" y2="30.48" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="30.48" x2="-3.81" y2="31.75" width="0.2032" layer="21"/>
<text x="-2.54" y="32.385" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-12.7" size="1.27" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<pad name="3.3V3" x="-7.62" y="-6.35" drill="0.9652"/>
<pad name="38/A19" x="7.62" y="-16.51" drill="0.9652"/>
<pad name="39/A20" x="7.62" y="-13.97" drill="0.9652"/>
<pad name="A21" x="7.62" y="-11.43" drill="0.9652"/>
<pad name="A22" x="7.62" y="-8.89" drill="0.9652"/>
<pad name="GND1" x="7.62" y="-6.35" drill="0.9652"/>
<pad name="28" x="-7.62" y="-19.05" drill="0.9652"/>
<pad name="29" x="-7.62" y="-21.59" drill="0.9652"/>
<pad name="30" x="-7.62" y="-24.13" drill="0.9652"/>
<pad name="31/A12" x="-7.62" y="-26.67" drill="0.9652"/>
<pad name="32/A13" x="-7.62" y="-29.21" drill="0.9652"/>
<pad name="33/A14" x="7.62" y="-29.21" drill="0.9652"/>
<pad name="37/A18" x="7.62" y="-19.05" drill="0.9652"/>
<pad name="36/A17" x="7.62" y="-21.59" drill="0.9652"/>
<pad name="35/A16" x="7.62" y="-24.13" drill="0.9652"/>
<pad name="34/A15" x="7.62" y="-26.67" drill="0.9652"/>
</package>
</packages>
<symbols>
<symbol name="TEENSY_3.5_DIL">
<wire x1="-20.32" y1="-53.34" x2="20.32" y2="-53.34" width="0.254" layer="94"/>
<wire x1="20.32" y1="-53.34" x2="20.32" y2="53.34" width="0.254" layer="94"/>
<wire x1="20.32" y1="53.34" x2="-20.32" y2="53.34" width="0.254" layer="94"/>
<wire x1="-20.32" y1="53.34" x2="-20.32" y2="-53.34" width="0.254" layer="94"/>
<pin name="12/MISO0" x="-25.4" y="20.32" visible="pin" length="middle"/>
<pin name="11/MOSI0" x="-25.4" y="22.86" visible="pin" length="middle"/>
<pin name="10/TX2/PWM" x="-25.4" y="25.4" visible="pin" length="middle"/>
<pin name="9/RX2/PWM" x="-25.4" y="27.94" visible="pin" length="middle"/>
<pin name="8/TX3/PWM" x="-25.4" y="30.48" visible="pin" length="middle"/>
<pin name="7/RX3/PWM" x="-25.4" y="33.02" visible="pin" length="middle"/>
<pin name="6/PWM" x="-25.4" y="35.56" visible="pin" length="middle"/>
<pin name="5/PWM" x="-25.4" y="38.1" visible="pin" length="middle"/>
<pin name="4/CAN0-RX/SDA2/PWM" x="-25.4" y="40.64" visible="pin" length="middle"/>
<pin name="3/CAN0-TX/SCL2/PWM" x="-25.4" y="43.18" visible="pin" length="middle"/>
<pin name="2/PWM" x="-25.4" y="45.72" visible="pin" length="middle"/>
<pin name="1/TX1/MISO1" x="-25.4" y="48.26" visible="pin" length="middle"/>
<pin name="0/RX1/MOSI1" x="-25.4" y="50.8" visible="pin" length="middle"/>
<pin name="GND" x="25.4" y="40.64" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="VIN" x="25.4" y="48.26" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="AGND" x="25.4" y="17.78" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="3.3V" x="25.4" y="45.72" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="23/A9/PWM" x="-25.4" y="-7.62" visible="pin" length="middle"/>
<pin name="22/A8/PWM" x="-25.4" y="-5.08" visible="pin" length="middle"/>
<pin name="21/A7/PWM" x="-25.4" y="-2.54" visible="pin" length="middle"/>
<pin name="20/A6/PWM" x="-25.4" y="0" visible="pin" length="middle"/>
<pin name="19/A5/SCL0" x="-25.4" y="2.54" visible="pin" length="middle"/>
<pin name="18/A4/SDA0" x="-25.4" y="5.08" visible="pin" length="middle"/>
<pin name="17/A3/PWM" x="-25.4" y="7.62" visible="pin" length="middle"/>
<pin name="16/A2" x="-25.4" y="10.16" visible="pin" length="middle"/>
<pin name="15/A1" x="-25.4" y="12.7" visible="pin" length="middle"/>
<pin name="14/A0/PWM" x="-25.4" y="15.24" visible="pin" length="middle"/>
<pin name="13/SCK0/LED" x="-25.4" y="17.78" visible="pin" length="middle"/>
<text x="-5.588" y="54.61" size="1.27" layer="95" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.794" y="-55.88" size="1.27" layer="96" font="vector" ratio="15">&gt;VALUE</text>
<pin name="24" x="-25.4" y="-10.16" visible="pin" length="middle"/>
<pin name="25" x="-25.4" y="-12.7" visible="pin" length="middle"/>
<pin name="26" x="-25.4" y="-15.24" visible="pin" length="middle"/>
<pin name="27" x="-25.4" y="-17.78" visible="pin" length="middle"/>
<pin name="28" x="-25.4" y="-20.32" visible="pin" length="middle"/>
<pin name="29/PWM" x="-25.4" y="-22.86" visible="pin" length="middle"/>
<pin name="30/PWM" x="-25.4" y="-25.4" visible="pin" length="middle"/>
<pin name="31/A12/RX4" x="-25.4" y="-27.94" visible="pin" length="middle"/>
<pin name="32/A13/TX4/SCK1" x="-25.4" y="-30.48" visible="pin" length="middle"/>
<pin name="33/A14/TX5" x="-25.4" y="-33.02" visible="pin" length="middle"/>
<pin name="A22/DAC1" x="25.4" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="A21/DAC0" x="25.4" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="34/A15/RX5" x="-25.4" y="-35.56" visible="pin" length="middle"/>
<pin name="35/A16/PWM" x="-25.4" y="-38.1" visible="pin" length="middle"/>
<pin name="36/A17/PWM" x="-25.4" y="-40.64" visible="pin" length="middle"/>
<pin name="37/A18/SCL1/PWM" x="-25.4" y="-43.18" visible="pin" length="middle"/>
<pin name="38/A19/SDA1/PWM" x="-25.4" y="-45.72" visible="pin" length="middle"/>
<pin name="39/A20" x="-25.4" y="-48.26" visible="pin" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEENSY_3.5_DIL">
<description>DIL version of the Teensy 3.5</description>
<gates>
<gate name="G$1" symbol="TEENSY_3.5_DIL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TEENSY_3.5_DIL">
<connects>
<connect gate="G$1" pin="0/RX1/MOSI1" pad="0"/>
<connect gate="G$1" pin="1/TX1/MISO1" pad="1"/>
<connect gate="G$1" pin="10/TX2/PWM" pad="10"/>
<connect gate="G$1" pin="11/MOSI0" pad="11"/>
<connect gate="G$1" pin="12/MISO0" pad="12"/>
<connect gate="G$1" pin="13/SCK0/LED" pad="13"/>
<connect gate="G$1" pin="14/A0/PWM" pad="14/A0"/>
<connect gate="G$1" pin="15/A1" pad="15/A1"/>
<connect gate="G$1" pin="16/A2" pad="16/A2"/>
<connect gate="G$1" pin="17/A3/PWM" pad="17/A3"/>
<connect gate="G$1" pin="18/A4/SDA0" pad="18/A4"/>
<connect gate="G$1" pin="19/A5/SCL0" pad="19/A5"/>
<connect gate="G$1" pin="2/PWM" pad="2"/>
<connect gate="G$1" pin="20/A6/PWM" pad="20/A6"/>
<connect gate="G$1" pin="21/A7/PWM" pad="21/A7"/>
<connect gate="G$1" pin="22/A8/PWM" pad="22/A8"/>
<connect gate="G$1" pin="23/A9/PWM" pad="23/A9"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29/PWM" pad="29"/>
<connect gate="G$1" pin="3.3V" pad="3.3V 3.3V3"/>
<connect gate="G$1" pin="3/CAN0-TX/SCL2/PWM" pad="3"/>
<connect gate="G$1" pin="30/PWM" pad="30"/>
<connect gate="G$1" pin="31/A12/RX4" pad="31/A12"/>
<connect gate="G$1" pin="32/A13/TX4/SCK1" pad="32/A13"/>
<connect gate="G$1" pin="33/A14/TX5" pad="33/A14"/>
<connect gate="G$1" pin="34/A15/RX5" pad="34/A15"/>
<connect gate="G$1" pin="35/A16/PWM" pad="35/A16"/>
<connect gate="G$1" pin="36/A17/PWM" pad="36/A17"/>
<connect gate="G$1" pin="37/A18/SCL1/PWM" pad="37/A18"/>
<connect gate="G$1" pin="38/A19/SDA1/PWM" pad="38/A19"/>
<connect gate="G$1" pin="39/A20" pad="39/A20"/>
<connect gate="G$1" pin="4/CAN0-RX/SDA2/PWM" pad="4"/>
<connect gate="G$1" pin="5/PWM" pad="5"/>
<connect gate="G$1" pin="6/PWM" pad="6"/>
<connect gate="G$1" pin="7/RX3/PWM" pad="7"/>
<connect gate="G$1" pin="8/TX3/PWM" pad="8"/>
<connect gate="G$1" pin="9/RX2/PWM" pad="9"/>
<connect gate="G$1" pin="A21/DAC0" pad="A21"/>
<connect gate="G$1" pin="A22/DAC1" pad="A22"/>
<connect gate="G$1" pin="AGND" pad="AGND"/>
<connect gate="G$1" pin="GND" pad="GND GND1"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ic-package">
<description>&lt;b&gt;IC Packages an Sockets&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL40">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="25.4" y1="6.731" x2="-25.4" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-6.731" x2="25.4" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="25.4" y1="6.731" x2="25.4" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="6.731" x2="-25.4" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-6.731" x2="-25.4" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.889" x2="-25.4" y2="-1.143" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-24.13" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-21.59" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-19.05" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-16.51" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="16.51" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="19.05" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="21.59" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="24.13" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="24.13" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="21.59" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="19.05" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="16.51" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="29" x="3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="30" x="1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="31" x="-1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="32" x="-3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="33" x="-6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="34" x="-8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="35" x="-11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="36" x="-13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="37" x="-16.51" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="38" x="-19.05" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="39" x="-21.59" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="40" x="-24.13" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-25.908" y="-6.604" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="-17.145" y="-1.016" size="1.778" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DIL40">
<wire x1="-5.08" y1="24.13" x2="-5.08" y2="-26.67" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-26.67" x2="5.08" y2="-26.67" width="0.254" layer="94"/>
<wire x1="5.08" y1="-26.67" x2="5.08" y2="24.13" width="0.254" layer="94"/>
<wire x1="5.08" y1="24.13" x2="2.54" y2="24.13" width="0.254" layer="94"/>
<wire x1="-5.08" y1="24.13" x2="-2.54" y2="24.13" width="0.254" layer="94"/>
<wire x1="-2.54" y1="24.13" x2="2.54" y2="24.13" width="0.254" layer="94" curve="180"/>
<text x="-4.445" y="24.765" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.445" y="-29.21" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="22.86" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-7.62" y="20.32" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-7.62" y="17.78" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-7.62" y="15.24" visible="pad" length="short" direction="pas"/>
<pin name="5" x="-7.62" y="12.7" visible="pad" length="short" direction="pas"/>
<pin name="6" x="-7.62" y="10.16" visible="pad" length="short" direction="pas"/>
<pin name="7" x="-7.62" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="8" x="-7.62" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="9" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="10" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="11" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="12" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="13" x="-7.62" y="-7.62" visible="pad" length="short" direction="pas"/>
<pin name="14" x="-7.62" y="-10.16" visible="pad" length="short" direction="pas"/>
<pin name="15" x="-7.62" y="-12.7" visible="pad" length="short" direction="pas"/>
<pin name="16" x="-7.62" y="-15.24" visible="pad" length="short" direction="pas"/>
<pin name="17" x="-7.62" y="-17.78" visible="pad" length="short" direction="pas"/>
<pin name="18" x="-7.62" y="-20.32" visible="pad" length="short" direction="pas"/>
<pin name="19" x="-7.62" y="-22.86" visible="pad" length="short" direction="pas"/>
<pin name="20" x="-7.62" y="-25.4" visible="pad" length="short" direction="pas"/>
<pin name="21" x="7.62" y="-25.4" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="22" x="7.62" y="-22.86" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="23" x="7.62" y="-20.32" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="24" x="7.62" y="-17.78" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="25" x="7.62" y="-15.24" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="26" x="7.62" y="-12.7" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="27" x="7.62" y="-10.16" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="28" x="7.62" y="-7.62" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="29" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="30" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="31" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="32" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="33" x="7.62" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="34" x="7.62" y="7.62" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="35" x="7.62" y="10.16" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="36" x="7.62" y="12.7" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="37" x="7.62" y="15.24" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="38" x="7.62" y="17.78" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="39" x="7.62" y="20.32" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="40" x="7.62" y="22.86" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIL40" prefix="IC" uservalue="yes">
<description>&lt;b&gt;Dual In Line&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DIL40" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIL40">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<description>&lt;b&gt;Jumpers&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SJ">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SJW">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51" curve="180"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="SJW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="headers(2mm)">
<packages>
<package name="2X20">
<rectangle x1="-19.254" y1="-1.254" x2="-18.746" y2="-0.746" layer="21"/>
<rectangle x1="-17.254" y1="-1.254" x2="-16.746" y2="-0.746" layer="21"/>
<rectangle x1="-15.254" y1="-1.254" x2="-14.746" y2="-0.746" layer="21"/>
<rectangle x1="-13.254" y1="-1.254" x2="-12.746" y2="-0.746" layer="21"/>
<rectangle x1="-11.254" y1="-1.254" x2="-10.746" y2="-0.746" layer="21"/>
<rectangle x1="-9.254" y1="-1.254" x2="-8.746" y2="-0.746" layer="21"/>
<rectangle x1="-7.254" y1="-1.254" x2="-6.746" y2="-0.746" layer="21"/>
<rectangle x1="-5.254" y1="-1.254" x2="-4.746" y2="-0.746" layer="21"/>
<rectangle x1="-3.254" y1="-1.254" x2="-2.746" y2="-0.746" layer="21"/>
<rectangle x1="-1.254" y1="-1.254" x2="-0.746" y2="-0.746" layer="21"/>
<rectangle x1="-19.254" y1="0.746" x2="-18.746" y2="1.254" layer="21"/>
<rectangle x1="-17.254" y1="0.746" x2="-16.746" y2="1.254" layer="21"/>
<rectangle x1="-15.254" y1="0.746" x2="-14.746" y2="1.254" layer="21"/>
<rectangle x1="-13.254" y1="0.746" x2="-12.746" y2="1.254" layer="21"/>
<rectangle x1="-11.254" y1="0.746" x2="-10.746" y2="1.254" layer="21"/>
<rectangle x1="-9.254" y1="0.746" x2="-8.746" y2="1.254" layer="21"/>
<rectangle x1="-7.254" y1="0.746" x2="-6.746" y2="1.254" layer="21"/>
<rectangle x1="-5.254" y1="0.746" x2="-4.746" y2="1.254" layer="21"/>
<rectangle x1="-3.254" y1="0.746" x2="-2.746" y2="1.254" layer="21"/>
<rectangle x1="-1.254" y1="0.746" x2="-0.746" y2="1.254" layer="21"/>
<pad name="1" x="-19" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="2" x="-19" y="1" drill="0.8" diameter="1.4224"/>
<pad name="3" x="-17" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="4" x="-17" y="1" drill="0.8" diameter="1.4224"/>
<pad name="5" x="-15" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="6" x="-15" y="1" drill="0.8" diameter="1.4224"/>
<pad name="7" x="-13" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="8" x="-13" y="1" drill="0.8" diameter="1.4224"/>
<pad name="9" x="-11" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="10" x="-11" y="1" drill="0.8" diameter="1.4224"/>
<pad name="11" x="-9" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="12" x="-9" y="1" drill="0.8" diameter="1.4224"/>
<pad name="13" x="-7" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="14" x="-7" y="1" drill="0.8" diameter="1.4224"/>
<pad name="15" x="-5" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="16" x="-5" y="1" drill="0.8" diameter="1.4224"/>
<pad name="17" x="-3" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="18" x="-3" y="1" drill="0.8" diameter="1.4224"/>
<pad name="19" x="-1" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="20" x="-1" y="1" drill="0.8" diameter="1.4224"/>
<rectangle x1="0.746" y1="-1.254" x2="1.254" y2="-0.746" layer="21"/>
<rectangle x1="2.746" y1="-1.254" x2="3.254" y2="-0.746" layer="21"/>
<rectangle x1="4.746" y1="-1.254" x2="5.254" y2="-0.746" layer="21"/>
<rectangle x1="6.746" y1="-1.254" x2="7.254" y2="-0.746" layer="21"/>
<rectangle x1="8.746" y1="-1.254" x2="9.254" y2="-0.746" layer="21"/>
<rectangle x1="10.746" y1="-1.254" x2="11.254" y2="-0.746" layer="21"/>
<rectangle x1="12.746" y1="-1.254" x2="13.254" y2="-0.746" layer="21"/>
<rectangle x1="14.746" y1="-1.254" x2="15.254" y2="-0.746" layer="21"/>
<rectangle x1="16.746" y1="-1.254" x2="17.254" y2="-0.746" layer="21"/>
<rectangle x1="18.746" y1="-1.254" x2="19.254" y2="-0.746" layer="21"/>
<rectangle x1="0.746" y1="0.746" x2="1.254" y2="1.254" layer="21"/>
<rectangle x1="2.746" y1="0.746" x2="3.254" y2="1.254" layer="21"/>
<rectangle x1="4.746" y1="0.746" x2="5.254" y2="1.254" layer="21"/>
<rectangle x1="6.746" y1="0.746" x2="7.254" y2="1.254" layer="21"/>
<rectangle x1="8.746" y1="0.746" x2="9.254" y2="1.254" layer="21"/>
<rectangle x1="10.746" y1="0.746" x2="11.254" y2="1.254" layer="21"/>
<rectangle x1="12.746" y1="0.746" x2="13.254" y2="1.254" layer="21"/>
<rectangle x1="14.746" y1="0.746" x2="15.254" y2="1.254" layer="21"/>
<rectangle x1="16.746" y1="0.746" x2="17.254" y2="1.254" layer="21"/>
<rectangle x1="18.746" y1="0.746" x2="19.254" y2="1.254" layer="21"/>
<pad name="21" x="1" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="22" x="1" y="1" drill="0.8" diameter="1.4224"/>
<pad name="23" x="3" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="24" x="3" y="1" drill="0.8" diameter="1.4224"/>
<pad name="25" x="5" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="26" x="5" y="1" drill="0.8" diameter="1.4224"/>
<pad name="27" x="7" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="28" x="7" y="1" drill="0.8" diameter="1.4224"/>
<pad name="29" x="9" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="30" x="9" y="1" drill="0.8" diameter="1.4224"/>
<pad name="31" x="11" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="32" x="11" y="1" drill="0.8" diameter="1.4224"/>
<pad name="33" x="13" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="34" x="13" y="1" drill="0.8" diameter="1.4224"/>
<pad name="35" x="15" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="36" x="15" y="1" drill="0.8" diameter="1.4224"/>
<pad name="37" x="17" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="38" x="17" y="1" drill="0.8" diameter="1.4224"/>
<pad name="39" x="19" y="-1" drill="0.8" diameter="1.4224"/>
<pad name="40" x="19" y="1" drill="0.8" diameter="1.4224"/>
</package>
</packages>
<symbols>
<symbol name="2X20_HEADER">
<wire x1="-5.08" y1="12.7" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-3.81" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="1.27" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-15.24" x2="-5.08" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-17.78" x2="-5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.32" x2="-5.08" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-22.86" x2="-5.08" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-25.4" x2="-5.08" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-27.94" x2="-5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-30.48" x2="-5.08" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-33.02" x2="-5.08" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-35.56" x2="-5.08" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-38.1" x2="-5.08" y2="-40.64" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-40.64" x2="2.54" y2="-40.64" width="0.254" layer="94"/>
<wire x1="2.54" y1="-40.64" x2="2.54" y2="-38.1" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.016" layer="94" font="vector" align="center">01</text>
<text x="0" y="10.16" size="1.016" layer="94" font="vector" align="center">02</text>
<text x="-5.08" y="13.335" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-5.08" y="-42.545" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<pin name="1" x="-5.08" y="10.16" visible="off" length="point"/>
<pin name="2" x="2.54" y="10.16" visible="off" length="point" rot="R180"/>
<wire x1="2.54" y1="-38.1" x2="2.54" y2="-35.56" width="0.254" layer="94"/>
<wire x1="2.54" y1="-35.56" x2="2.54" y2="-33.02" width="0.254" layer="94"/>
<wire x1="2.54" y1="-33.02" x2="2.54" y2="-30.48" width="0.254" layer="94"/>
<wire x1="2.54" y1="-30.48" x2="2.54" y2="-27.94" width="0.254" layer="94"/>
<wire x1="2.54" y1="-27.94" x2="2.54" y2="-25.4" width="0.254" layer="94"/>
<wire x1="2.54" y1="-25.4" x2="2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="2.54" y1="-22.86" x2="2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="2.54" y2="-17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-3.81" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="1.27" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.016" layer="94" font="vector" align="center">03</text>
<text x="0" y="7.62" size="1.016" layer="94" font="vector" align="center">04</text>
<pin name="3" x="-5.08" y="7.62" visible="off" length="point"/>
<pin name="4" x="2.54" y="7.62" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.016" layer="94" font="vector" align="center">05</text>
<text x="0" y="5.08" size="1.016" layer="94" font="vector" align="center">06</text>
<pin name="5" x="-5.08" y="5.08" visible="off" length="point"/>
<pin name="6" x="2.54" y="5.08" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.016" layer="94" font="vector" align="center">07</text>
<text x="0" y="2.54" size="1.016" layer="94" font="vector" align="center">08</text>
<pin name="7" x="-5.08" y="2.54" visible="off" length="point"/>
<pin name="8" x="2.54" y="2.54" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.016" layer="94" font="vector" align="center">09</text>
<text x="0" y="0" size="1.016" layer="94" font="vector" align="center">10</text>
<pin name="9" x="-5.08" y="0" visible="off" length="point"/>
<pin name="10" x="2.54" y="0" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="0" y="-2.54" size="1.016" layer="94" font="vector" align="center">12</text>
<text x="-2.54" y="-5.08" size="1.016" layer="94" font="vector" align="center">13</text>
<pin name="11" x="-5.08" y="-2.54" visible="off" length="point"/>
<pin name="12" x="2.54" y="-2.54" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="-5.08" x2="-3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<text x="0" y="-5.08" size="1.016" layer="94" font="vector" align="center">14</text>
<text x="-2.54" y="-7.62" size="1.016" layer="94" font="vector" align="center">15</text>
<pin name="13" x="-5.08" y="-5.08" visible="off" length="point"/>
<pin name="14" x="2.54" y="-5.08" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="-7.62" x2="-3.81" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="1.27" y2="-7.62" width="0.254" layer="94"/>
<text x="0" y="-7.62" size="1.016" layer="94" font="vector" align="center">16</text>
<text x="-2.54" y="-10.16" size="1.016" layer="94" font="vector" align="center">17</text>
<pin name="15" x="-5.08" y="-7.62" visible="off" length="point"/>
<pin name="16" x="2.54" y="-7.62" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="-10.16" x2="-3.81" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="1.27" y2="-10.16" width="0.254" layer="94"/>
<text x="0" y="-10.16" size="1.016" layer="94" font="vector" align="center">18</text>
<text x="-2.54" y="-12.7" size="1.016" layer="94" font="vector" align="center">19</text>
<pin name="17" x="-5.08" y="-10.16" visible="off" length="point"/>
<pin name="18" x="2.54" y="-10.16" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="-12.7" x2="-3.81" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="1.27" y2="-12.7" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.016" layer="94" font="vector" align="center">11</text>
<text x="0" y="-12.7" size="1.016" layer="94" font="vector" align="center">20</text>
<pin name="19" x="-5.08" y="-12.7" visible="off" length="point"/>
<pin name="20" x="2.54" y="-12.7" visible="off" length="point" rot="R180"/>
<wire x1="-5.08" y1="-15.24" x2="-3.81" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-17.78" x2="-3.81" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-20.32" x2="-3.81" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-22.86" x2="-3.81" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-25.4" x2="-3.81" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-27.94" x2="-3.81" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-30.48" x2="-3.81" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-33.02" x2="-3.81" y2="-33.02" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="1.27" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="1.27" y2="-17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="1.27" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-22.86" x2="1.27" y2="-22.86" width="0.254" layer="94"/>
<wire x1="2.54" y1="-25.4" x2="1.27" y2="-25.4" width="0.254" layer="94"/>
<wire x1="2.54" y1="-27.94" x2="1.27" y2="-27.94" width="0.254" layer="94"/>
<wire x1="2.54" y1="-30.48" x2="1.27" y2="-30.48" width="0.254" layer="94"/>
<wire x1="2.54" y1="-33.02" x2="1.27" y2="-33.02" width="0.254" layer="94"/>
<text x="-2.54" y="-15.24" size="1.016" layer="94" font="vector" align="center">21</text>
<text x="-2.54" y="-17.78" size="1.016" layer="94" font="vector" align="center">23</text>
<text x="-2.54" y="-20.32" size="1.016" layer="94" font="vector" align="center">25</text>
<text x="-2.54" y="-22.86" size="1.016" layer="94" font="vector" align="center">27</text>
<text x="-2.54" y="-25.4" size="1.016" layer="94" font="vector" align="center">29</text>
<text x="-2.54" y="-27.94" size="1.016" layer="94" font="vector" align="center">31</text>
<text x="-2.54" y="-30.48" size="1.016" layer="94" font="vector" align="center">33</text>
<text x="-2.54" y="-33.02" size="1.016" layer="94" font="vector" align="center">35</text>
<text x="0" y="-15.24" size="1.016" layer="94" font="vector" align="center">22</text>
<text x="0" y="-17.78" size="1.016" layer="94" font="vector" align="center">24</text>
<text x="0" y="-20.32" size="1.016" layer="94" font="vector" align="center">26</text>
<text x="0" y="-22.86" size="1.016" layer="94" font="vector" align="center">28</text>
<text x="0" y="-25.4" size="1.016" layer="94" font="vector" align="center">30</text>
<text x="0" y="-27.94" size="1.016" layer="94" font="vector" align="center">32</text>
<text x="0" y="-30.48" size="1.016" layer="94" font="vector" align="center">34</text>
<text x="0" y="-33.02" size="1.016" layer="94" font="vector" align="center">36</text>
<wire x1="-5.08" y1="-35.56" x2="-3.81" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-38.1" x2="-3.81" y2="-38.1" width="0.254" layer="94"/>
<wire x1="2.54" y1="-35.56" x2="1.27" y2="-35.56" width="0.254" layer="94"/>
<wire x1="2.54" y1="-38.1" x2="1.27" y2="-38.1" width="0.254" layer="94"/>
<text x="-2.54" y="-35.56" size="1.016" layer="94" font="vector" align="center">37</text>
<text x="-2.54" y="-38.1" size="1.016" layer="94" font="vector" align="center">39</text>
<text x="0" y="-35.56" size="1.016" layer="94" font="vector" align="center">38</text>
<text x="0" y="-38.1" size="1.016" layer="94" font="vector" align="center">40</text>
<pin name="21" x="-5.08" y="-15.24" visible="off" length="point"/>
<pin name="22" x="2.54" y="-15.24" visible="off" length="point" rot="R180"/>
<pin name="23" x="-5.08" y="-17.78" visible="off" length="point"/>
<pin name="24" x="2.54" y="-17.78" visible="off" length="point" rot="R180"/>
<pin name="25" x="-5.08" y="-20.32" visible="off" length="point"/>
<pin name="26" x="2.54" y="-20.32" visible="off" length="point" rot="R180"/>
<pin name="27" x="-5.08" y="-22.86" visible="off" length="point"/>
<pin name="28" x="2.54" y="-22.86" visible="off" length="point" rot="R180"/>
<pin name="29" x="-5.08" y="-25.4" visible="off" length="point"/>
<pin name="30" x="2.54" y="-25.4" visible="off" length="point" rot="R180"/>
<pin name="31" x="-5.08" y="-27.94" visible="off" length="point"/>
<pin name="32" x="2.54" y="-27.94" visible="off" length="point" rot="R180"/>
<pin name="33" x="-5.08" y="-30.48" visible="off" length="point"/>
<pin name="34" x="2.54" y="-30.48" visible="off" length="point" rot="R180"/>
<pin name="35" x="-5.08" y="-33.02" visible="off" length="point"/>
<pin name="36" x="2.54" y="-33.02" visible="off" length="point" rot="R180"/>
<pin name="37" x="-5.08" y="-35.56" visible="off" length="point"/>
<pin name="38" x="2.54" y="-35.56" visible="off" length="point" rot="R180"/>
<pin name="39" x="-5.08" y="-38.1" visible="off" length="point"/>
<pin name="40" x="2.54" y="-38.1" visible="off" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2X20">
<gates>
<gate name="G$1" symbol="2X20_HEADER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X20">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="TEENSY" library="Teensy_3_and_LC_Series_Boards_v1.4" deviceset="TEENSY_3.5_DIL" device=""/>
<part name="5380" library="ic-package" deviceset="DIL40" device="" value="NCR"/>
<part name="TERMPWR" library="jumper" deviceset="SJ" device="W"/>
<part name="SCSI" library="headers(2mm)" deviceset="2X20" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="TEENSY" gate="G$1" x="66.04" y="83.82"/>
<instance part="5380" gate="G$1" x="142.24" y="83.82"/>
<instance part="TERMPWR" gate="1" x="144.78" y="119.38"/>
<instance part="SCSI" gate="G$1" x="205.74" y="109.22"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="200.66" y1="114.3" x2="193.04" y2="114.3" width="0.1524" layer="91"/>
<label x="193.04" y="114.3" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="5"/>
</segment>
<segment>
<wire x1="200.66" y1="111.76" x2="193.04" y2="111.76" width="0.1524" layer="91"/>
<label x="193.04" y="111.76" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="7"/>
</segment>
<segment>
<wire x1="200.66" y1="109.22" x2="193.04" y2="109.22" width="0.1524" layer="91"/>
<label x="193.04" y="109.22" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="9"/>
</segment>
<segment>
<wire x1="200.66" y1="99.06" x2="193.04" y2="99.06" width="0.1524" layer="91"/>
<label x="193.04" y="99.06" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="17"/>
</segment>
<segment>
<wire x1="200.66" y1="101.6" x2="193.04" y2="101.6" width="0.1524" layer="91"/>
<label x="193.04" y="101.6" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="15"/>
</segment>
<segment>
<wire x1="200.66" y1="104.14" x2="193.04" y2="104.14" width="0.1524" layer="91"/>
<label x="193.04" y="104.14" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="13"/>
</segment>
<segment>
<wire x1="200.66" y1="106.68" x2="193.04" y2="106.68" width="0.1524" layer="91"/>
<label x="193.04" y="106.68" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="11"/>
</segment>
<segment>
<wire x1="200.66" y1="96.52" x2="193.04" y2="96.52" width="0.1524" layer="91"/>
<label x="193.04" y="96.52" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="19"/>
</segment>
<segment>
<wire x1="200.66" y1="93.98" x2="193.04" y2="93.98" width="0.1524" layer="91"/>
<label x="193.04" y="93.98" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="21"/>
</segment>
<segment>
<wire x1="200.66" y1="91.44" x2="193.04" y2="91.44" width="0.1524" layer="91"/>
<label x="193.04" y="91.44" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="23"/>
</segment>
<segment>
<wire x1="200.66" y1="86.36" x2="193.04" y2="86.36" width="0.1524" layer="91"/>
<label x="193.04" y="86.36" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="27"/>
</segment>
<segment>
<wire x1="200.66" y1="81.28" x2="193.04" y2="81.28" width="0.1524" layer="91"/>
<label x="193.04" y="81.28" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="31"/>
</segment>
<segment>
<wire x1="200.66" y1="76.2" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
<label x="193.04" y="76.2" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="35"/>
</segment>
<segment>
<wire x1="134.62" y1="81.28" x2="127" y2="81.28" width="0.1524" layer="91"/>
<label x="127" y="81.28" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="11"/>
</segment>
<segment>
<pinref part="TEENSY" gate="G$1" pin="GND"/>
<wire x1="91.44" y1="124.46" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<label x="93.98" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TEENSY" gate="G$1" pin="AGND"/>
<wire x1="91.44" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<label x="93.98" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="215.9" y1="116.84" x2="208.28" y2="116.84" width="0.1524" layer="91"/>
<label x="210.82" y="116.84" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="200.66" y1="116.84" x2="193.04" y2="116.84" width="0.1524" layer="91"/>
<label x="193.04" y="116.84" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="215.9" y1="73.66" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<label x="210.82" y="73.66" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="38"/>
</segment>
<segment>
<wire x1="200.66" y1="73.66" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<label x="193.04" y="73.66" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="37"/>
</segment>
</net>
<net name="DB0" class="0">
<segment>
<wire x1="208.28" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<label x="210.82" y="114.3" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="6"/>
</segment>
<segment>
<wire x1="127" y1="86.36" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
<label x="127" y="86.36" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="9"/>
</segment>
</net>
<net name="DB1" class="0">
<segment>
<wire x1="208.28" y1="111.76" x2="215.9" y2="111.76" width="0.1524" layer="91"/>
<label x="210.82" y="111.76" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="8"/>
</segment>
<segment>
<wire x1="127" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<label x="127" y="88.9" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="8"/>
</segment>
</net>
<net name="DB2" class="0">
<segment>
<wire x1="208.28" y1="109.22" x2="215.9" y2="109.22" width="0.1524" layer="91"/>
<label x="210.82" y="109.22" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="10"/>
</segment>
<segment>
<wire x1="127" y1="91.44" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<label x="127" y="91.44" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="7"/>
</segment>
</net>
<net name="DB3" class="0">
<segment>
<wire x1="208.28" y1="106.68" x2="215.9" y2="106.68" width="0.1524" layer="91"/>
<label x="210.82" y="106.68" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="12"/>
</segment>
<segment>
<wire x1="127" y1="93.98" x2="134.62" y2="93.98" width="0.1524" layer="91"/>
<label x="127" y="93.98" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="6"/>
</segment>
</net>
<net name="DB4" class="0">
<segment>
<wire x1="208.28" y1="104.14" x2="215.9" y2="104.14" width="0.1524" layer="91"/>
<label x="210.82" y="104.14" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="14"/>
</segment>
<segment>
<wire x1="127" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<label x="127" y="96.52" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="5"/>
</segment>
</net>
<net name="DB5" class="0">
<segment>
<wire x1="208.28" y1="101.6" x2="215.9" y2="101.6" width="0.1524" layer="91"/>
<label x="210.82" y="101.6" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="16"/>
</segment>
<segment>
<wire x1="127" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<label x="127" y="99.06" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="4"/>
</segment>
</net>
<net name="DB6" class="0">
<segment>
<wire x1="208.28" y1="99.06" x2="215.9" y2="99.06" width="0.1524" layer="91"/>
<label x="210.82" y="99.06" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="18"/>
</segment>
<segment>
<wire x1="127" y1="101.6" x2="134.62" y2="101.6" width="0.1524" layer="91"/>
<label x="127" y="101.6" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="3"/>
</segment>
</net>
<net name="DB7" class="0">
<segment>
<wire x1="208.28" y1="96.52" x2="215.9" y2="96.52" width="0.1524" layer="91"/>
<label x="210.82" y="96.52" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="20"/>
</segment>
<segment>
<wire x1="127" y1="104.14" x2="134.62" y2="104.14" width="0.1524" layer="91"/>
<label x="127" y="104.14" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="2"/>
</segment>
</net>
<net name="DBP" class="0">
<segment>
<wire x1="208.28" y1="93.98" x2="215.9" y2="93.98" width="0.1524" layer="91"/>
<label x="210.82" y="93.98" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="22"/>
</segment>
<segment>
<wire x1="127" y1="83.82" x2="134.62" y2="83.82" width="0.1524" layer="91"/>
<label x="127" y="83.82" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="10"/>
</segment>
</net>
<net name="TRM" class="0">
<segment>
<wire x1="208.28" y1="91.44" x2="215.9" y2="91.44" width="0.1524" layer="91"/>
<label x="210.82" y="91.44" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="24"/>
</segment>
<segment>
<wire x1="149.86" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<label x="152.4" y="119.38" size="1.778" layer="95"/>
<pinref part="TERMPWR" gate="1" pin="2"/>
</segment>
</net>
<net name="ATN" class="0">
<segment>
<wire x1="200.66" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<label x="193.04" y="88.9" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="25"/>
</segment>
<segment>
<wire x1="127" y1="71.12" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<label x="127" y="71.12" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="15"/>
</segment>
</net>
<net name="BSY" class="0">
<segment>
<wire x1="208.28" y1="88.9" x2="215.9" y2="88.9" width="0.1524" layer="91"/>
<label x="210.82" y="88.9" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="26"/>
</segment>
<segment>
<wire x1="127" y1="76.2" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<label x="127" y="76.2" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="13"/>
</segment>
</net>
<net name="ACK" class="0">
<segment>
<wire x1="208.28" y1="86.36" x2="215.9" y2="86.36" width="0.1524" layer="91"/>
<label x="210.82" y="86.36" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="28"/>
</segment>
<segment>
<wire x1="127" y1="73.66" x2="134.62" y2="73.66" width="0.1524" layer="91"/>
<label x="127" y="73.66" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="14"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<wire x1="193.04" y1="83.82" x2="200.66" y2="83.82" width="0.1524" layer="91"/>
<label x="193.04" y="83.82" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="29"/>
</segment>
<segment>
<wire x1="127" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
<label x="127" y="68.58" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="16"/>
</segment>
</net>
<net name="MSG" class="0">
<segment>
<wire x1="208.28" y1="83.82" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<label x="210.82" y="83.82" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="30"/>
</segment>
<segment>
<wire x1="127" y1="60.96" x2="134.62" y2="60.96" width="0.1524" layer="91"/>
<label x="127" y="60.96" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="19"/>
</segment>
</net>
<net name="SEL" class="0">
<segment>
<wire x1="208.28" y1="81.28" x2="215.9" y2="81.28" width="0.1524" layer="91"/>
<label x="210.82" y="81.28" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="32"/>
</segment>
<segment>
<wire x1="127" y1="78.74" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<label x="127" y="78.74" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="12"/>
</segment>
</net>
<net name="CD" class="0">
<segment>
<wire x1="208.28" y1="78.74" x2="215.9" y2="78.74" width="0.1524" layer="91"/>
<label x="210.82" y="78.74" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="34"/>
</segment>
<segment>
<wire x1="127" y1="63.5" x2="134.62" y2="63.5" width="0.1524" layer="91"/>
<label x="127" y="63.5" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="18"/>
</segment>
</net>
<net name="REQ" class="0">
<segment>
<wire x1="208.28" y1="76.2" x2="215.9" y2="76.2" width="0.1524" layer="91"/>
<label x="210.82" y="76.2" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="36"/>
</segment>
<segment>
<wire x1="127" y1="58.42" x2="134.62" y2="58.42" width="0.1524" layer="91"/>
<label x="127" y="58.42" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="20"/>
</segment>
</net>
<net name="IO" class="0">
<segment>
<wire x1="193.04" y1="78.74" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
<label x="193.04" y="78.74" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="33"/>
</segment>
<segment>
<wire x1="127" y1="66.04" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<label x="127" y="66.04" size="1.778" layer="95"/>
<pinref part="5380" gate="G$1" pin="17"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="1"/>
<wire x1="134.62" y1="106.68" x2="127" y2="106.68" width="0.1524" layer="91"/>
<label x="127" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="40.64" y1="81.28" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<label x="35.56" y="81.28" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="21/A7/PWM"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="40"/>
<wire x1="149.86" y1="106.68" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<label x="152.4" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="83.82" x2="40.64" y2="83.82" width="0.1524" layer="91"/>
<label x="35.56" y="83.82" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="20/A6/PWM"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="39"/>
<wire x1="149.86" y1="104.14" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
<label x="152.4" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="119.38" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="6/PWM"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="38"/>
<wire x1="149.86" y1="101.6" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<label x="152.4" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
<label x="35.56" y="114.3" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="8/TX3/PWM"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="37"/>
<wire x1="149.86" y1="99.06" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<label x="152.4" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="116.84" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<label x="35.56" y="116.84" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="7/RX3/PWM"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="36"/>
<wire x1="149.86" y1="96.52" x2="157.48" y2="96.52" width="0.1524" layer="91"/>
<label x="152.4" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="40.64" x2="40.64" y2="40.64" width="0.1524" layer="91"/>
<label x="35.56" y="40.64" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="37/A18/SCL1/PWM"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="35"/>
<wire x1="149.86" y1="93.98" x2="157.48" y2="93.98" width="0.1524" layer="91"/>
<label x="152.4" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="43.18" x2="40.64" y2="43.18" width="0.1524" layer="91"/>
<label x="35.56" y="43.18" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="36/A17/PWM"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="34"/>
<wire x1="149.86" y1="91.44" x2="157.48" y2="91.44" width="0.1524" layer="91"/>
<label x="152.4" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="45.72" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
<label x="35.56" y="45.72" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="35/A16/PWM"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="33"/>
<wire x1="149.86" y1="88.9" x2="157.48" y2="88.9" width="0.1524" layer="91"/>
<label x="152.4" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="101.6" x2="40.64" y2="101.6" width="0.1524" layer="91"/>
<label x="35.56" y="101.6" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="13/SCK0/LED"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="32"/>
<wire x1="149.86" y1="86.36" x2="157.48" y2="86.36" width="0.1524" layer="91"/>
<label x="152.4" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="106.68" x2="40.64" y2="106.68" width="0.1524" layer="91"/>
<label x="35.56" y="106.68" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="11/MOSI0"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="31"/>
<wire x1="149.86" y1="83.82" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<label x="152.4" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TEENSY" gate="G$1" pin="VIN"/>
<wire x1="91.44" y1="132.08" x2="99.06" y2="132.08" width="0.1524" layer="91"/>
<label x="93.98" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="132.08" y1="119.38" x2="139.7" y2="119.38" width="0.1524" layer="91"/>
<label x="132.08" y="119.38" size="1.778" layer="95"/>
<pinref part="TERMPWR" gate="1" pin="1"/>
</segment>
<segment>
<wire x1="193.04" y1="119.38" x2="200.66" y2="119.38" width="0.1524" layer="91"/>
<label x="193.04" y="119.38" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="208.28" y1="119.38" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<label x="210.82" y="119.38" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="193.04" y1="71.12" x2="200.66" y2="71.12" width="0.1524" layer="91"/>
<label x="193.04" y="71.12" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="39"/>
</segment>
<segment>
<wire x1="208.28" y1="71.12" x2="215.9" y2="71.12" width="0.1524" layer="91"/>
<label x="210.82" y="71.12" size="1.778" layer="95"/>
<pinref part="SCSI" gate="G$1" pin="40"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="30"/>
<wire x1="149.86" y1="81.28" x2="157.48" y2="81.28" width="0.1524" layer="91"/>
<label x="152.4" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="104.14" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
<label x="35.56" y="104.14" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="12/MISO0"/>
</segment>
</net>
<net name="IOW" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="29"/>
<wire x1="149.86" y1="78.74" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<label x="152.4" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="38.1" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<label x="35.56" y="38.1" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="38/A19/SDA1/PWM"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="28"/>
<wire x1="149.86" y1="76.2" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<label x="152.4" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
<label x="35.56" y="93.98" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="16/A2"/>
</segment>
</net>
<net name="EOP" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="27"/>
<wire x1="149.86" y1="73.66" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<label x="152.4" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="91.44" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<label x="35.56" y="91.44" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="17/A3/PWM"/>
</segment>
</net>
<net name="DACK" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="26"/>
<wire x1="149.86" y1="71.12" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="152.4" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="129.54" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<label x="35.56" y="129.54" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="2/PWM"/>
</segment>
</net>
<net name="READY" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="25"/>
<wire x1="149.86" y1="68.58" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<label x="152.4" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="86.36" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<label x="35.56" y="86.36" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="19/A5/SCL0"/>
</segment>
</net>
<net name="IOR" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="24"/>
<wire x1="149.86" y1="66.04" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<label x="152.4" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="58.42" x2="40.64" y2="58.42" width="0.1524" layer="91"/>
<label x="35.56" y="58.42" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="30/PWM"/>
</segment>
</net>
<net name="IRQ" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="23"/>
<wire x1="149.86" y1="63.5" x2="157.48" y2="63.5" width="0.1524" layer="91"/>
<label x="152.4" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
<label x="35.56" y="48.26" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="34/A15/RX5"/>
</segment>
</net>
<net name="DRQ" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="22"/>
<wire x1="149.86" y1="60.96" x2="157.48" y2="60.96" width="0.1524" layer="91"/>
<label x="152.4" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="50.8" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
<label x="35.56" y="50.8" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="33/A14/TX5"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="5380" gate="G$1" pin="21"/>
<wire x1="149.86" y1="58.42" x2="157.48" y2="58.42" width="0.1524" layer="91"/>
<label x="152.4" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="99.06" x2="40.64" y2="99.06" width="0.1524" layer="91"/>
<label x="35.56" y="99.06" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="14/A0/PWM"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="TEENSY" gate="G$1" pin="3.3V"/>
<wire x1="91.44" y1="129.54" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
<label x="93.98" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="OSI" class="0">
<segment>
<wire x1="40.64" y1="134.62" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<label x="33.02" y="134.62" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="0/RX1/MOSI1"/>
</segment>
</net>
<net name="OCK" class="0">
<segment>
<wire x1="40.64" y1="53.34" x2="33.02" y2="53.34" width="0.1524" layer="91"/>
<label x="33.02" y="53.34" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="32/A13/TX4/SCK1"/>
</segment>
</net>
<net name="OCS" class="0">
<segment>
<wire x1="40.64" y1="55.88" x2="33.02" y2="55.88" width="0.1524" layer="91"/>
<label x="33.02" y="55.88" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="31/A12/RX4"/>
</segment>
</net>
<net name="ORS" class="0">
<segment>
<wire x1="40.64" y1="66.04" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<label x="33.02" y="66.04" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="27"/>
</segment>
</net>
<net name="ODC" class="0">
<segment>
<wire x1="40.64" y1="63.5" x2="33.02" y2="63.5" width="0.1524" layer="91"/>
<label x="33.02" y="63.5" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="28"/>
</segment>
</net>
<net name="OSO" class="0">
<segment>
<wire x1="40.64" y1="132.08" x2="33.02" y2="132.08" width="0.1524" layer="91"/>
<label x="33.02" y="132.08" size="1.778" layer="95"/>
<pinref part="TEENSY" gate="G$1" pin="1/TX1/MISO1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,91.44,132.08,TEENSY,VIN,VDD,,,"/>
<approved hash="104,1,91.44,101.6,TEENSY,AGND,GND,,,"/>
<approved hash="104,1,91.44,129.54,TEENSY,3.3V,3V3,,,"/>
<approved hash="113,1,144.972,120.896,TERMPWR,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
